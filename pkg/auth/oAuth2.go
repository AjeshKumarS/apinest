package auth

import (
	"net/http"
	"strings"
)

// Google OAuth 2.0
func GoogleOAuth2(strict ...bool) func(next http.Handler) http.Handler {
	// Configured middleware
	return func(next http.Handler) http.Handler {
		fn := func(res http.ResponseWriter, req *http.Request) {
			authHeader := req.Header.Get("Authorization")

			// Test if authorization header present
			if len(authHeader) > 0 {
				tokenArray := strings.Split(authHeader, " ")

				// Test if the header is in the form Bearer<space><token>
				if len(tokenArray) < 2 {
					res.WriteHeader(401)
					message := []byte("Invalid Authorization Header")
					res.Write(message)
					return
				}

				// Call google api endpoint to validate the id token
				oAuthResponse, err := http.Get("https://oauth2.googleapis.com/tokeninfo?id_token=" + tokenArray[1])

				// If request fails
				if err != nil {
					res.WriteHeader(500)
					message := []byte("Authorization call failed")
					res.Write(message)
					return
				}

				// If the request status code != 200 the token is invalid
				if oAuthResponse.StatusCode != 200 {
					res.WriteHeader(401)
					message := []byte("Invalid IdToken")
					res.Write(message)
					return
				}
			} else if len(strict) > 0 && strict[0] { // make sure the authorization header is present in strict mode
				res.WriteHeader(401)
				message := []byte("Unauthenticated")
				res.Write(message)
				return
			}
			// If none of the above cases apply, the authorization is done and call the next handler
			next.ServeHTTP(res, req)
		}
		return http.HandlerFunc(fn)
	}
}
