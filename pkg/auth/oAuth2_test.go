package auth

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// Test for Google OAuth2
func TestGoogleOAuth2(t *testing.T) {

	// Next handler function if the middleware executes without error
	handlerFunc := http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		t.Error("Google OAuth middleware failed to block request with invalid header")
	})

	testSet := getOauth2TestSet()

	// Run each testcase
	for _, testCase := range testSet {
		// create a mock request to use
		req := httptest.NewRequest("GET", "http://testing", nil)
		// create mock response writer
		res := httptest.NewRecorder()
		// Add authorization header
		req.Header.Set("Authorization", testCase.headerValue)

		// Create google oauth middleware
		oAuthMiddleware := GoogleOAuth2(testCase.strict)

		// Call the middleware passing next handlerFunc
		handlerToTest := oAuthMiddleware(handlerFunc)

		handlerToTest.ServeHTTP(res, req)
		result := res.Result()

		// validate response status code
		if result.StatusCode != testCase.expectedStatusCode {
			t.Error("Expected status code: " + strconv.Itoa(testCase.expectedStatusCode) + ", got: " + strconv.Itoa(result.StatusCode))
		}

		body := res.Body.String()
		// validate response body
		if body != testCase.expectedBody {
			t.Error("Expected body: \"" + testCase.expectedBody + "\", got: \"" + body + "\"")
		}
	}
}

// Test Case struct
type Oauth2TestCase struct {
	strict             bool
	headerValue        string
	expectedStatusCode int
	expectedBody       string
}

// Create different testcases
func getOauth2TestSet() []Oauth2TestCase {
	testSet := []Oauth2TestCase{
		{false, "sample", 401, "Invalid Authorization Header"},
		{false, "Bearer sample", 401, "Invalid IdToken"},
		{true, "", 401, "Unauthenticated"},
	}
	return testSet
}
