module gitlab.com/apinest/apinest

go 1.15

require (
	github.com/TwinProduction/go-color v1.0.0
	github.com/auth0-community/go-auth0 v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/sys v0.0.0-20210223095934-7937bea0104d // indirect
	golang.org/x/text v0.3.4 // indirect
)
