**<u>Header Transformation</u>** - For request and response
`append|overwrite|remove:header.headername`

**1. Filter**

- **Allow** - to only allow in the request the headers you explicitly list (any other headers are removed from the request).
- **Block** - to remove from the request the headers you explicitly list.

```json
"filterHeaders": {
    "type": "BLOCK",
    "items": [
        {
            "name": "User-Agent"
        }
    ]
}
```

**2. Rename:**

- **"from":** `<original-name>` is the original name of the header that you are renaming. The name you specify is not case-sensitive, and must not be included in any other transformation request policies for the route. For example, `X-Username`.
- **"to":** `<new-name>` is the new name of the header you are renaming. The name you specify is not case-sensitive (capitalization might be ignored), and must not be included in any other transformation request policies for the route (with the exception of items in ALLOW lists). For example, `X-User-ID`.

```json
"renameHeaders": {
    "items": [
        {
            "from": "<original-name>",
            "to": "<new-name>"
        }
    ]
}
```

**3. Add:**

- To add a new header to a request (or to change or retain the values of an existing header already included in a request), specify a `setHeaders` header transformation request policy:

```json
"setHeaders": {
    "items": [
        {
        "name": "<header-name>",
        "values": ["<header-value>"],
        "ifExists": "<OVERWRITE|APPEND|SKIP>"
        }
    ]
}
```

<br>

**_References:_**

1. https://docs.oracle.com/en-us/iaas/Content/APIGateway/Tasks/apigatewaymodifyingresponsesrequests.htm#addingheadertransformrequestpolicies__usingjsonrequest
2. https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-parameter-mapping.html
