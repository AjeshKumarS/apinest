package main

import (
	"net/http"

	"gitlab.com/apinest/apinest/pkg/nestLogger"

	"gitlab.com/apinest/apinest/pkg/middlewares"
	"gitlab.com/apinest/apinest/pkg/nestRouter"
	"gitlab.com/apinest/apinest/pkg/reverseProxy"
)

// Sample function
func Hello() string {
	return "Hello World!"
}

const (
	MEMES_SERVER   = "http://memes-service.herokuapp.com"
	HEROES_SERVER  = "http://heroes-service.herokuapp.com"
	THREATS_SERVER = "http://threats-service.herokuapp.com"
)

// Main function Entry point of program
func main() {
	commonMiddlewares := middlewares.New()
	commonMiddlewares.Add(nestLogger.RequestLogger)
	ConfigureMiddlewares(&commonMiddlewares)
	route1Middlewares := middlewares.New(commonMiddlewares)
	route1Middlewares.Add(CustomMiddleware3)
	// route1Middlewares.Add(auth.Auth0Middleware)

	var router = nestRouter.NewRouter()

	router.HandleFunc("/sample", reverseProxy.HandleRequestAndRedirect(MEMES_SERVER))

	router.Handle("/memes", commonMiddlewares.ThenFunc(reverseProxy.HandleRequestAndRedirect(MEMES_SERVER)))
	router.Handle("/heroes", commonMiddlewares.ThenFunc(reverseProxy.HandleRequestAndRedirect(HEROES_SERVER)))
	router.Handle("/threats", commonMiddlewares.ThenFunc(reverseProxy.HandleRequestAndRedirect(THREATS_SERVER)))
	// router.Handle("/1", route1Middlewares.ThenFunc(reverseProxy.HandleRequestAndRedirect(MEMES_SERVER)))

	http.Handle("/", router.Exec())

	nestLogger.Log("Proxy Server Listening on http://localhost:8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
