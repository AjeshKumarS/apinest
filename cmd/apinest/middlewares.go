package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/apinest/apinest/pkg/middlewares"
)

// Configure the common middlewares for the api gateway
func ConfigureMiddlewares(collection *middlewares.Collection) {
	collection.Add(CustomMiddleware1)
	collection.Add(CustomMiddleware2)
}

// Sample middleware 1
func CustomMiddleware1(next http.Handler) http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {
		fmt.Println("Executing middleware 1...")
		time.Sleep(1 * time.Second)
		next.ServeHTTP(res, req)
	}
	return http.HandlerFunc(fn)
}

// Sample middleware 2
func CustomMiddleware2(next http.Handler) http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {
		fmt.Println("Executing middleware 2...")
		time.Sleep(1 * time.Second)
		next.ServeHTTP(res, req)
	}
	return http.HandlerFunc(fn)
}

// Sample middleware 3
func CustomMiddleware3(next http.Handler) http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {
		fmt.Println("Executing middleware 3...")
		time.Sleep(1 * time.Second)
		next.ServeHTTP(res, req)
	}
	return http.HandlerFunc(fn)
}
