package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type Auth0Params struct {
	Auth0ApiAudience []string
	Auth0Domain      string
}

type Response struct {
	Message string `json:"message"`
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

func (a *Auth0Params) getPemCert(token *jwt.Token) (string, error) {
	fmt.Println("Getting PEM certificate...")
	cert := ""
	resp, err := http.Get(fmt.Sprintf("%s.well-known/jwks.json", a.Auth0Domain))

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for _, key := range jwks.Keys {
		if token.Header["kid"] == key.Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + key.X5c[0] + "\n-----END CERTIFICATE-----"
			break
		}
	}

	if cert == "" {
		err := fmt.Errorf("unable to find appropriate key")
		return cert, err
	}

	return cert, nil
}

func (a *Auth0Params) verifyPayload(parsedToken *jwt.Token) error {
	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return fmt.Errorf("invalid claims")
	}

	// Verify user ('aud' claim)
	checkAud := claims.VerifyAudience(a.Auth0ApiAudience[0], false)
	if !checkAud {
		return fmt.Errorf("invalid audience")
	}

	// Verify 'iss' claim
	iss := a.Auth0Domain
	checkIss := parsedToken.Claims.(jwt.MapClaims).VerifyIssuer(iss, false)
	if !checkIss {
		return fmt.Errorf("invalid issuer")
	}
	return nil
}

func (a *Auth0Params) verifyJWT(tokenString string) error {
	fmt.Println("Executing verifyJWT()...")
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		ok := token.Valid
		if !ok {
			return nil, fmt.Errorf("error parsing the token")
		}

		cert, err := a.getPemCert(token)
		fmt.Println("cert: \n", cert)
		if err != nil {
			return "", fmt.Errorf("error getting pem certificate: %s", err)
		}

		publicKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
		if err != nil {
			return "", fmt.Errorf("error loading public key: %s", err)
		}

		fmt.Println("publicKey: ", publicKey)
		return publicKey, nil
	}
	parsedToken, _ := jwt.Parse(tokenString, keyFunc)
	if parsedToken == nil {
		return fmt.Errorf("nil parsedToken")
	}

	fmt.Printf("parsedToken: %v\n", parsedToken)

	// Verify header.
	if parsedToken.Header["alg"] != "RS256" {
		return fmt.Errorf("algorithm doesn't match")
	}

	// Verify payload.
	err := a.verifyPayload(parsedToken)
	if err != nil {
		return fmt.Errorf("error verifying payload: %s", err)
	}
	return nil
}

// Auth0
func Auth0(authParams Auth0Params, strict ...bool) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(res http.ResponseWriter, req *http.Request) {
			authHeader := req.Header.Get("Authorization")

			fmt.Printf("\nauthHeader: %s\n", authHeader)
			// check if authorization header is present and is valid
			if len(authHeader) > 0 {
				// Create a configuration with the Auth0 information
				tokenArray := strings.Split(authHeader, " ")
				// Test if the header is in the form Bearer<space><token>
				if len(tokenArray) < 2 || tokenArray[0] != "Bearer" {
					res.WriteHeader(401)
					message := []byte("Invalid Authorization Header")
					res.Write(message)
					return
				}

				err := authParams.verifyJWT(tokenArray[1])

				fmt.Println("Completed verifyJWT()")

				// If the token has been revoked or verification failed
				if err != nil {
					res.WriteHeader(401)
					fmt.Println("Error verifying JWT: ", err)
					message := []byte("Invalid IdToken")
					res.Write(message)
					return
				}

				fmt.Println("token valid")
			} else if len(strict) > 0 && strict[0] { // => default mode is non-strict
				// if not present, and the mode is strict, then error (Header is not compulsory in non-strict mode).
				res.WriteHeader(401)
				message := []byte("Unauthenticated")
				res.Write(message)
				return
			}
			next.ServeHTTP(res, req)
		}
		return http.HandlerFunc(fn)
	}
}

// const domain = "https://dev-apinest.us.auth0.com/"

// var ap = Auth0Params{
// 	Auth0ApiAudience: []string{fmt.Sprintf("%sapi/v2/", domain), fmt.Sprintf("%suserinfo", domain)},
// 	Auth0Domain:      domain,
// }
// var Auth0Middleware = Auth0(ap, false)

// {domain}/pem
// {domain}/.well-known/jwks.json
